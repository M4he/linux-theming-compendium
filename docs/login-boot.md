This chapter will only briefly touch on the topics of login and boot screen theming to point you into the right direction since the details are heavily dependent on your distribution and environment.

> **NOTE:** Detailed instructions and settings of specific DMs and boot sequences is out of scope for this reference guide.
Please consult to your distribution's documentation, each tool's manpages or the excellent Arch Wiki for further details.

## Login Screen

The process of theming your login screen depends on which display manager (DM) you are using, since that component actually displays the login screen.
You can usually mix and match DMs with DEs/WMs as desired.
However KDE and GNOME prefer their corresponding DM (SDDM for KDE and GDM for GNOME respectively) for best integration. For example, you won't be able to lock the screen on GNOME if not using GDM.

### <a name="lightdm"></a> LightDM

A universal DM often used by Ubuntu flavors and many smaller distributions.
It offers a variety of theme engines called "greeters" from which can be selected.
Greeters are essentially themes with some settings.
You can mostly retrieve them from packages named `lightdm-*-greeter`.

For easy configuration there are GUI tools like `lightdm-settings` and `lightdm-gtk-greeter-settings`.
Though the latter only applies to the GTK greeter.

The basic configuration is done via `/etc/lightdm/lightdm.conf`.
Here you can also change the greeter:

```
[Seat:*]
...
greeter-session=lightdm-webkit-greeter
...
```

The greeters themselves have dedicated configuration files in `/etc/lightdm/`.
For example, the GTK greeter can be configured with `/etc/lightdm/lightdm-gtk-greeter.conf`.
You can mostly find instructions about all possible configuration settings of a greeter in its configuration file as comments.

#### Hardcoding display configuration in LightDM

LightDM will attempt to enable and use all connected displays when starting up, which might not be desirable.

You can force a configuration by using `xrandr` commands in `/etc/lightdm/lightdm.conf` like so:

```
[Seat:*]
...
display-setup-script=xrandr --output HDMI-1 --auto --primary --output HDMI-2 --off
```

### <a name="sddm"></a> SDDM

The primary DM of KDE/Plasma and LXQt.

SDDM an be configured via GUI using `systemsettings` if you have KDE/Plasma installed.
In all other cases, you can create `*.conf` configuration files in `/etc/sddm.conf.d/`.
For available options (including theming) refer to `man sddm.conf`.
Its themes reside in `/usr/share/sddm/themes/` and can be selected using said configuration files.

#### Hardcoding display configuration in SDDM

SDDM will attempt to enable and use all connected displays when starting up, which might not be desirable.

You can force a configuration by using `xrandr` commands in a dedicated `/usr/share/sddm/scripts/Xsetup` file:

```bash
xrandr --output HDMI-1 --auto --primary --output HDMI-2 --off
```

### <a name="gdm"></a> GDM

The primary DM of GNOME.
It's theming is tightly coupled to the settings of GNOME itself.

You can configure parts of it with `dconf-editor` under the dconf path `/org/gnome/login-screen`.
Theming settings like icon theme or font settings are in sync with the GNOME settings and are retrieved from the dconf path `/org/gnome/desktop/interface` instead, which you can also adjust with `dconf-editor`.

## <a name="plymouth"></a> Boot Screen

For a graphical bootscreen with logo and/or animation, you'll need **Plymouth**.
The installation and setup process depends on your distribution.
The following sample will illustrate the process for Debian-based distributions, where Plymouth is often already pre-installed.

#### Prerequirements

When using GRUB, make sure that your kernel line contains the `splash` parameter.
For Debian systems, this resides in the file `/etc/defaults/grub`:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash ..."
```

(the three dots `...` are just a visual placeholder for any additional options in your config in this example, *do not* include them in your file!)

Make sure to run the following command as root after making changes to `/etc/defaults/grub`:

```bash
update-grub
```

#### Getting themes

Debian-based distributions already provide packages with themes named: `plymouth-themes` or `plymouth-theme-*`.
You can also install any downloaded themes to `/usr/share/plymouth/themes/`.

#### Setting the Plymouth theme

> **HINT:** If you have a UEFI-based system, using the special theme "**bgrt**" will incorporate the logo of your device/mainboard manufacturer as the boot splash.

As the root user, execute the following command while replacing `<theme-name>` with the name of any of the folders within `/usr/share/plymouth/themes`

```bash
plymouth-set-default-theme -R <theme-name>
```