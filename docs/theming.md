## Compositor

For elements in GTK and Qt to render properly (especially things with transparency or rounded corners, menus and popups), you will need a compositor.
Also, to get proper screen refresh and VSync (preventing screen tearing and stutter) you will want a compositor in general.

Full-fledged DEs like KDE/Plasma, GNOME, Budgie, MATE, Xfce\* ship their own compositor.

picom ([GitHub link](https://github.com/yshui/picom)) is quite universal and can provide compositing for DEs/WMs not providing it, e.g. Openbox, LXQt, awesome.

> \***HINT:** You can disable Xfwm4's compositing in "Window Manager Tweaks" > "Compositor" and use `picom` with Xfwm4 instead.

### Prevent double-shadows on GTK3 windows when using picom

GTK3 will render its own shadows.
If you use picom with shadows enabled, shadows will be rendered twice.
To fix that, exclude `_GTK_FRAME_EXTENTS@:c` from shadows in your picom configuration:

```
shadow-exclude = [
    "_GTK_FRAME_EXTENTS@:c",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_MAXIMIZED'",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_FULLSCREEN'"
];
```

> **NOTE:** The `_NET_WM_STATE` lines are not part of the fix for the double shadows but will remove shadows from fullscreen/maximized applications, which is a good idea in general.

## Theming GTK2/3 outside of major GTK-focused DEs

### <a name="xsettings-interface"></a> The XSETTINGS interface

Most major GTK-focused desktop environments use a background service that implements the XSETTINGS standard.
For example, in GNOME this is done by `gnome-settings-daemon` and settings are managed via `dconf`.
In Xfce those are `xfsettingsd` and `xfconf` respectively.
KDE/Plasma uses a custom instance of `xsettingsd` with a `~/.config/xsettingsd/xsettingsd.conf` generated from settings in KDE's `systemsettings`.
LXQt also uses `xsettingsd` (note [this FAQ entry](FAQ.md#faq-lxqt-xsettingsd) regarding LXQt).

GTK Applications will retrieve most theming information from the XSETTINGS interface if it is available.

> **NOTE:** The XSETTINGS interface will override settings in the [GTK RC files](#gtk-rc-files) if used!

> **NOTE:** Please note [this pitfall](FAQ.md#faq-mozilla-xdg-desktop-portal) regarding using the XSETTINGS interface with Firefox and Thunderbird!

#### <a name="xsettingsd-config"></a> Setting up and configuring `xsettingsd`

`xsettingsd` is a DE-agnostic service that provides the XSETTINGS interface for applications and can be configured with a simple configuration file.
It is the first choice for robust GTK theming outside of the major GTK-focused DEs.

1. Install `xsettingsd`.
2. Create a configuration file at `~/.xsettingsd` or a custom place.
3. Add `xsettingsd` (for `~/.xsettingsd`) or `xsettingsd -c /path/to/.xsettingsd` (for custom paths) to your autostart.

> **TIP:** You can use `killall -HUP xsettingsd` to make `xsettingsd` reload its configuration file and refresh all applications instantly!

The most important entries in the configuration file are:

```
...
Gtk/CursorThemeName "Breeze"
Gtk/CursorThemeSize 32
Gtk/FontName "Roboto 11"
Net/IconThemeName "Tela"
Net/ThemeName "Adwaita"
Xft/Antialias 1
Xft/DPI 98304
Xft/Hinting 1
Xft/HintStyle "hintslight"
Xft/RGBA "rgb"
```

(see the [full example](appendix.md#xsettingsd-conf-example) in the appendix for more options)

> **NOTE:** `Xft/DPI` is the actual DPI value multiplied by 1024!

> **NOTE:** Make sure that `Xft/DPI` (divided by 1024) is in sync with the `QT_FONT_DPI` environment variable (see "Theming KDE and Qt5") and that all `Xft/*` variables (including DPI) are also in sync with your `~/.config/fontconfig/fonts.conf`.

### GTK3: Removing the client-side decoration (CSD) header bars

Install `gtk3-nocsd`.
Upon the next login, it should set the environment variable `GTK_CSD=0` for you.
This will make GTK3 applications render with your window manager's titlebars instead.

### <a name="gtk-rc-files"></a> GTK RC Files

> **NOTE:** `xsettingsd` is the recommended way for GTK2/3 theming.
The GTK RC files are just mentioned here for the sake of completeness and as a fallback method.

> **NOTE:** The GTK RC files will be ignored if you have any service running which is providing the XSETTINGS interface, such as `xsettingsd`, see the [XSETTINGS interface](#xsettings-interface) section.

There are two configuration files for GTK applications to retrieve theming information from:

- `~/.gtkrc-2.0` for GTK2 applications
- `~/.config/gtk-3.0/settings.ini` for GTK3 applications

This is the fallback method outside of major DEs if you don't opt to use `xsettingsd` or a similar service.
Configuration tools like `lxappearance` or LXQt's GTK styling options will create these files for you.
You can omit/delete them if you use an appropriate XSETTINGS service.

## <a name="qt5ct"></a> Theming KDE applications and Qt5 outside of KDE/Plasma

### Basics

Install **qt5ct**. This will allow you to configure some specific settings for Qt5 applications and also change their theme.
Before using qt5ct, you need to set it as the Qt5 style globally by setting the `QT_QPA_PLATFORMTHEME` environment variable, see below.

#### <a name="qt5-envvars"></a> Environment Variables

Configuring KDE and Qt5 applications outside of KDE/Plasma requires a few environment variables to be set.
Depending on your environment, you might need to set those at different places:

- for Openbox, put those lines in `~/.config/openbox/environment`
- for LXQt, add the names and values to "Environment (Advanced)" in the `lxqt-config-session` dialog (omitting the `export`)
- for anything else, you can put those lines into your `~/.xsessionrc`, see [session startup files](advanced.md#session-startup)

The relevant environment variables are (including example values):

```bash
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_FONT_DPI=92
export QT_STYLE_OVERRIDE=Breeze
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
export SAL_USE_VCLPLUGIN=kf5
```

Explanation:

|            Variable           |     Type     |                                                                                                                          Description                                                                                                                          |
|-------------------------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `QT_QPA_PLATFORMTHEME`        | Essential    | Sets the main theme. Should always be set to `qt5ct` to enable proper theming control outside of KDE/Plasma.                                                                                                                                                  |
| `QT_FONT_DPI`                 | Essential    | Sets the font DPI for all Qt5 applications. **NOTE:** for best results, set this in sync with the values in the [XSETTINGS configuration](#xsettings-interface) and `~/.config/fontconfig/fonts.conf` of the [fontconfig](fonts-cursors-icons.md#fontconfig). |
| `QT_STYLE_OVERRIDE`           | Optional Fix | Fixes the "free space" progress bar in Dolphin's status bar. Might fix other styling issues too. Set this the same value as "Style:" in qt5ct.                                                                                                                |
| `QT_AUTO_SCREEN_SCALE_FACTOR` | Optional Fix | If set to `0`, prevents Qt5 from auto-scaling the UI depending on screen DPI. Fixes Okular being rendered in a different size and totally broken.                                                                                                             |
| `QT_SCALE_FACTOR`             | Optional Fix | Sets the UI scaling factor for Qt5. Use with `QT_AUTO_SCREEN_SCALE_FACTOR=0`. Helps fixing Okular.                                                                                                                                                            |
| `SAL_USE_VCLPLUGIN`           | Optional Fix | Sets the UI renderer of LibreOffice. Set this to `kf5` or `kde5` to use Qt5 and the KDE file picker dialog.                                                                                                                                                   |

> **NOTE:** Setting `SAL_USE_VCLPLUGIN=kf5` to change the LibreOffice UI rendering to KDE/Qt5 requires the package `libreoffice-kf5` or `libreoffice-kde5` to be installed (on Debian-based systems).
> When using LibreOffice packages from libreoffice.org directly this requires all the dependencies that said package would install, since it requires the necessary KF5 libraries on the system.
> If you don't want the KDE libraries, you can use `SAL_USE_VCLPLUGIN=qt5` with `SAL_VCL_QT5_USE_CAIRO=1` (always use both together) instead!

##### <a name="qt-systemd-styling"></a> systemd units

Although rare, some Qt applications might be started through a user-scoped systemd service that acts as a background service.
Such applications might not receive the environment variable values described above due to being started from a separate process that is not a descendant of the desktop session.
An example is `pinentry-qt` when it is used by the GPG agent systemd service for SSH key passphrase prompts.

Basically this can affect all Qt windows spawned by subprocesses of `systemd --user`.
To fix that, environment variables need to be set for systemd separately:

1. Create a file called `~/.config/environment.d/qt-styling.conf`. If the `~/.config/environment.d/` directory does not exist, create it first.
2. Into this file insert the environment variables as described above but without the `export` in front of them. For example:

    ```bash
    QT_QPA_PLATFORMTHEME=qt5ct
    QT_STYLE_OVERRIDE=Breeze
    ```

3. It is usually sufficient to use `systemd --user restart <servicename>` to reload the affected services but the changes should apply after the next reboot at the latest.

> **Tip:** if you are using `gpg-agent` in a Qt environment, install `pinentry-qt` and add "`pinentry-program /usr/bin/pinentry-qt`" to your `.gnupg/gpg-agent.conf` to enable native Qt dialogs for passphrase prompts.
> Using the fix described here, it will also be styled accordingly.

### <a name="qt5ct-color-scheme-tweak"></a> Tweak: qt5ct and KDE/Plasma color schemes

> **WARNING:** Having a dedicated qt5ct color scheme configuration is advised but does not replace settings in `~/.config/kdeglobals`.
Some applications are affected by both. Ensure that you keep both in sync when changing color schemes!
See [this FAQ entry](FAQ.md#faq-qt5ct-kdeglobals-sync).

If you opt to use a Qt style and color scheme from KDE/Plasma (e.g. Breeze) there are a few tweaks you need to apply in order to get accurate colors in all Qt5 applications with qt5ct:

1. Make sure qt5ct is configured correctly and the Breeze style is active, see above.
2. Make a backup of your `~/.config/kdeglobals`.
3. If you have KDE/Plasma installed fully, use `XDG_CURRENT_DESKTOP=KDE systemsettings` and configure the color scheme. Otherwise you may use the following command to manually apply a KDE color scheme (**WARNING:** will reset/overwrite some KDE/Plasma settings):
        
    ```bash
    cp /usr/share/color-schemes/<scheme-name>.colors ~/.config/kdeglobals
    ```

    (replace `<scheme-name>` with your desired color scheme name)

4. Open up qt5ct and select the "Style:", e.g. "Breeze".
5. Change the color scheme from "Default" to "Custom".
6. Click the "..." dropdown to the right of the color scheme name selector, choose "Create". Give it a suitable name, e.g. "Breeze Dark".
7. From the color scheme name selector dropdown, select the color scheme you just created.

Clicking "Create" in step 6 will effectively create a copy of the *currently applied* KDE/Plasma color set from `~/.config/kdeglobals` as a qt5ct color scheme stored in `~/.config/qt5ct/colors/`.

Using just the "Default" color setting in qt5ct may look right at a glance but some applications like QTerminal (tab color) or `lxqt-leave` (background color) might have wrong colors if not using a qt5ct-native copy of the colors.

> **NOTE:** If you opt to use Breeze as your GTK3 theme as well, you can also sync your color scheme to the GTK3 counterpart.
> See the instructions for [changing the colors of the GTK3 Breeze theme](advanced.md#breeze-gtk3-colors).

## Tips & Tricks

### <a name="gtk-font-tweak"></a> GTK renders font thinner than KDE/Qt

Qt renders fonts differently than GTK, which may lead to visual differences like fonts looking thinner in GTK on dark themes.
To solve that, you can use the "Medium" thickness of fonts that support it.
For example, for the font "Inter":

`~/.xsettingsd`
```
Gtk/FontName "InterMedium, 11"
```

> **NOTE:** It is advised to omit the space between the font name and "Medium" (e.g. "InterMedium" instead of "Inter Medium") for GTK, since especially GTK2 will have trouble with it.
Using "<fontname>Medium" without a space should work for both GTK 2 and 3.

#### Applying the tweak to KDE/Plasma

KDE/Plasma will create a `/.config/xsettingsd/xsettingsd.conf` configuration file for `xsettingsd` to apply its font settings to GTK application.
It will regularly check and overwrite this file, so simply adjusting it once is futile.
Instead, we need a login script to apply our tweak:

1. Create a script file called `~/.config/kde-patch-gtk-font.sh` with the following content:
        ```bash
        #!/bin/bash
        sed -i "s/Inter,/InterMedium,/" $HOME/.config/xsettingsd/xsettingsd.conf
        killall -HUP xsettingsd
        ```

2. Make the script executable with `chmod +x ~/.config/kde-patch-gtk-font.sh`
3. Open up system settings, navigate to "Startup and Shutdown" > "Autostart".
4. Use the "Add..." button and choose "Add Login Script...", select the script you just created.

Adjust the script according to your font name.
Remember to also update it if you choose to change the KDE/Plasma font later in the future.
Note the remark about the omitted space character between the font name and "Medium" in the above section.
