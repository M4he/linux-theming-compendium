### My mouse cursor has different sizes or themes between applications

- make sure your cursor settings in the `xsettingsd` configuration file and `~/.Xdefaults` are in sync
- make sure that `xrdb -merge $HOME/.Xdefaults` is in your autostart somewhere or gets loaded by your DM
- make sure that your cursor theme is installed system-wide in `/usr/share/icons/`

### <a name="faq-mozilla-xdg-desktop-portal"></a> Firefox and Thunderbird display wrong theme, icons and/or fonts outside of major DEs

When used outside of major DEs, you might observe Firefox and Thunderbird not respecting your theme settings.
In this case try to remove all of the following packages/services and restart your desktop session:

- `xdg-desktop-portal`
- `xdg-desktop-portal-kde`
- `xdg-desktop-portal-gtk`

The above mentioned services only offer proper theme integration for Firefox and Thunderbird under GNOME, KDE/Plasma or similar major DEs where they provide applications with interface configuration (including themes, icons and fonts) via a D-Bus interface.

Outside of compatible DEs these services will be autostarted by systemd but don't function properly, making Firefox and Thunderbird unable to retrieve the correct theming settings as both of them tend to ignore the [XSETTINGS interface](theming.md#xsettings-interface) and any `~/.config/gtk-3.0/settings.ini` configuration if they detect these services running.

### I'm using the same font for GTK and Qt but it still looks slightly different

If the size looks is different, make sure you use the same font DPI in all applicable configuration files, see [font configuration](fonts-cursors-icons.md#fontconfig).

If the size matches but the font seems to have different thickness between GTK and Qt, you may try the [font thickness tweak](theming.md#gtk-font-tweak).

### I'm using a dark KDE color scheme with qt5ct but some Qt elements appear bright or miscolored

qt5ct has inaccuracies in translating the native KDE/Plasma color scheme.
See the [corresponding tweak section](theming.md#qt5ct-color-scheme-tweak).

### Okular or another Qt5 application has weird font sizes and UI elements are too big and get cut off

Set the `QT_AUTO_SCREEN_SCALE_FACTOR` and `QT_SCALE_FACTOR` environment variables, see the [environment variables section](theming.md#qt5-envvars) for Qt5.

### KDE's Dolphin file manager has a weird glossy-looking "free space" indicator bar in its status bar on the bottom right

Try setting the `QT_STYLE_OVERRIDE` environment variable, see [here](theming.md#qt5-envvars).

### KDE's Plasma apps like `systemsettings` or `plasma-systemmonitor` look completely broken outside of KDE/Plasma

In addition to ensuring correct [environment variables for Qt](theming.md#qt5-envvars), try starting those apps with `XDG_CURRENT_DESKTOP=KDE` set, e.g.

```bash
XDG_CURRENT_DESKTOP=KDE systemsettings
```

### <a name="faq-qt5ct-kdeglobals-sync"></a> I changed the color scheme in qt5ct but colors in Qt applications are wrong in some places

Do note that for some KDE applications the qt5ct color scheme will not cover everything, especially if you use the Breeze Qt style.
Such applications also retrieve additional information from `~/.config/kdeglobals` and it is adviced to keep this file in sync with qt5ct.

To apply and sync the colors to qt5ct follow the instructions [described here](theming.md#qt5ct-color-scheme-tweak).
It is important that you keep the `~/.config/kdeglobals` after this and repeat the process when changing color schemes.

You might opt to create dynamically linked `kdeglobals` and `qt5ct` configurations (see [here](advanced.md#qt5ct-linking)) if you plan to switch between color schemes frequently.

### Qt passphrase prompts for GPG are not styled correctly when using `pinentry-qt`

If you have set "`pinentry-program /usr/bin/pinentry-qt`" in your `~/.gnupg/gpg-agent.conf` and defined a custom Qt styling outside of KDE/Plasma, you might run into an issue where the passphrase prompt is not styled correctly.

To solve this see the [systemd tweak for passing environment variables](theming.md#qt-systemd-styling).

### I used `lxappearance` / I modified `~/.gtkrc-2.0` or `~/.config/gtk-3.0/settings.ini` but changes don't apply

There might be an XSETTINGS service running in the background overruling those settings, see the [XSETTINGS section](theming.md#xsettings-interface).

### <a name="faq-lxqt-xsettingsd"></a> I've set up my own `xsettingsd` config on LXQt but my GTK theming gets messed up occasionally

Even if you disable "Set GTK themes" in LXQt's appearance settings, as soon as you open the appearance dialog of LXQt, it will start its own instance of `xsettingsd` in the background, replacing your custom instance.
In that case simply restart `xsettingsd` with your own configuration file again after closing the appearance dialog.

If you want to change your LXQt theme, you can circumvent the appearance dialog entirely by simply editing `~/.config/lxqt/lxqt.conf` and adjusting the `theme=` line.
Simply saving the file should instantly apply the new theme.

### <a name="faq-x11-icons"></a> The window icons on the panel / taskbar / window titlebars I'm using don't match my icon theme

This is a limitation of how window icons are handled by X11.
Some more advanced panels and docks (like those of the big DEs) will lookup the icon separately.
Other than that, each application specifies their X11 window icon on its own.
Some might adhere to your icon theme while doing that, some may not.
More basic panels (like tint2) and window managers (like Openbox) will show the X11 window icon set by the application.
You usually cannot change the icon the application sets for itself other than changing the application code.

Using icon changes in `.desktop` files will not affect X11 window icons.
However, *some* applications will set their X11 window icon based on your currently configured icon theme.
Thus, modifying your icon theme directly can help in some cases.