## Tweaking GTK3 applications with additional CSS

GTK3 is based on CSS and can be extended this way too.
You can specify overrides in `~/.config/gtk-3.0/gtk.css` that will take precedence over the active GTK3 theme.

> **HINT:** You can start any GTK3 application via `GTK_DEBUG=interactive <application>` and get an interactive debug UI where you can try identifying class names of various parts of an application to access them in CSS.

#### Example: add space around content in terminals

Most GTK3-based terminals will use VTE.
In your `~/.config/gtk-3.0/gtk.css` you can add the following to add additional space around the terminal content:

```css
VteTerminal, vte-terminal {
  padding: 4px;
}
```

#### Example: change padding in Thunar's bread-crumb path bar

```css
.thunar .path-bar-button {
    padding: 2px 6px 2px 6px;
}
```


### <a name="breeze-gtk3-colors"></a> Changing the GTK3 colors of Breeze

> **NOTE:** The following will only change colors for the GTK3 Breeze theme and won't affect the GTK2 counterpart due to its technical limitations.

Despite being a Qt style, Breeze also provides a matching GTK2+GTK3 theme.
For example, on Debian-based systems (including Ubuntu) this is provided by the package `gtk3-engines-breeze`.
The GTK3 theme of Breeze supports being recolored.
This is what KDE/Plasma uses to sync its color scheme to GTK3 applications if Breeze is used.
It is possible to manually recreate this behavior outside of KDE/Plasma.

> **NOTE:** If you use KDE/Plasma's control panel (`systemsettings`) to change the Qt color scheme, KDE/Plasma will overwrite `~/.config/gtk-3.0/colors.css` by itself.
> If you have KDE/Plasma installed, you can use that instead of configuring this manually as shown below.

1. Create `~/.config/gtk-3.0/gtk.css` if it doesn't exist and add the following line:

    ```
    @import 'colors.css';
    ```

2. Create `~/.config/gtk-3.0/colors.css` if it doesn't exist and insert the following content:

    ```
    @define-color theme_button_decoration_focus_backdrop_breeze #88cc00;
    @define-color theme_button_decoration_focus_backdrop_insensitive_breeze #88cc00;
    @define-color theme_button_decoration_focus_breeze #88cc00;
    @define-color theme_button_decoration_focus_insensitive_breeze #88cc00;
    @define-color theme_button_decoration_hover_backdrop_breeze #88cc00;
    @define-color theme_button_decoration_hover_backdrop_insensitive_breeze #88cc00;
    @define-color theme_button_decoration_hover_breeze #88cc00;
    @define-color theme_button_decoration_hover_insensitive_breeze #88cc00;
    @define-color theme_hovering_selected_bg_color_breeze #cce993;
    @define-color theme_selected_bg_color_breeze #88cc00;
    @define-color theme_view_active_decoration_color_breeze #88cc00;
    @define-color theme_view_hover_decoration_color_breeze #88cc00;
    ```

3. Replace `#88cc00` with an accent color of your choice, preferably matching your `kdeglobals`/`qt5ct` colors.

> **HINT:** The example above only changes accent colors.
> It is possible to adjust all other colors as well, see the full [example in the appendix](appendix.md#breeze-gtk3-colors).

You'll need to restart GTK3 applications for the new colors to apply after adjusting them.
Make sure your GTK3 is set to either "Breeze" or "Breeze-Dark" for this to work, e.g. as `Net/ThemeName` in your [`xsettingsd` configuration](theming.md#xsettings-interface).

## Per-Session Theming

Most tools described so far can be set up in a way that their configuration does not interfere with other sessions if you have several DEs/WMs installed.
You can check environment variables like `XDG_CURRENT_DESKTOP` or `XDG_SESSION_DESKTOP` within [session startup files](#session-startup) to figure out which DE/WM is running and redirect paths accordingly.
See the [example](appendix.md#xsessionrc-example) in the appendix below for illustration on how to use this.

### xsettingsd

`xsettingsd` supports specifying a path to the configuration file, so you can simply have dedicated configuration files per session and select them during startup.

Autostart example:

```bash
xsettingsd -c $HOME/.xsettingsd-openbox
```

### <a name="qt5ct-linking"></a> qt5ct, Breeze

A lot of Breeze configuration originates from `~/.config/kdeglobals`, which you can turn into a symlink.
The additional configuration for qt5ct resides in `~/.config/qt5ct/`, which is a directory but can be a symlink as well.

```bash
# move existing configuration to new name
mv ~/.config/kdeglobals ~/.config/kdeglobals.default
mv ~/.config/qt5ct/ ~/.config/qt5ct.default/
# turn into symlinks pointing to new names
ln -sfn ~/.config/kdeglobals.default ~/.config/kdeglobals
ln -sfn ~/.config/qt5ct.default/ ~/.config/qt5ct/
```

Both of them are now symlinks and behave just as before:

```
~/.config/qt5ct -> ~/.config/qt5ct.default
~/.config/kdeglobals -> ~/.config/kdeglobals.default
```

The advantage of symlinks is, you can relink them anytime using `ln -sfn` without moving actual files around, so you can switch over to different files by relinking them during login:

```bash
# create empty if not existing
touch $HOME/.config/kdeglobals.custom
mkdir -p $HOME/.config/qt5ct.custom
# relink
ln -sfn $HOME/.config/kdeglobals.custom $HOME/.config/kdeglobals
ln -sfn $HOME/.config/qt5ct.custom $HOME/.config/qt5ct
```

See [the example](appendix.md#xsessionrc-example) seeing this in action using the [session startup mechanism](#session-startup) described below.

### nitrogen

> **NOTE:** nitrogen will not accept symlinks of its configuration.
Thus, a different approach is necessary.

Nitrogen uses the `XDG_CONFIG_HOME` environment variable.
You can redirect the configuration location by setting this environment variable in front of the `nitrogen` command in your autostart and script commands.

Autostart example:

```bash
DISPLAY=:0 XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen \
    --force-setter=xinerama --restore
```

Setting a wallpaper via nitrogen's GUI:

```bash
DISPLAY=:0 XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen \
    --force-setter=xinerama
```

Setting a wallpaper on the command line or in a script:

```bash
export MONITOR=0
DISPLAY=:0 XDG_CONFIG_HOME=$HOME/.config/openbox nitrogen \
    --force-setter=xinerama --save --head=$MONITOR --set-zoom-fill "$1"
```

`$1` is the first argument passed to a script, which should be the file path to the image.

> **NOTE:** The `--force-setter=xinerama` parameter is for proper multi-screen support with current Intel iGPUs.
You might need a different `--force-setter` parameter depending on your environment. See `nitrogen --help`.

## <a name="session-startup"></a> Session Startup Files

The following files can be used as autostart scripts independently of the DM or WM used.

> **NOTE:** Most desktop environments (DEs) and some window managers (WMs) offer their own autostart mechanisms.
The files stated below will be active *additionally* and be executed before the DE-/DM-specific ones.

If you use any DM (e.g. lightdm, sddm etc.) you will want to opt for [`~/.xsessionrc`](#xsessionrc).

### <a name="xinitrc"></a> The `~/.xinitrc`

- executed by `xinit`
    - usually invoked by `startx`
- for setting up GUI-related settings
    - e.g. `xrdb`, `xmodmap` or `xkbcomp`

> First you log in on a text console, then you start the GUI with `startx`.

### <a name="xsessionrc"></a> The `~/.xsessionrc`

- executed by X startup scripts on a GUI login
    - applies to _all_ session types and Display Managers
- also executed from `startx` *if* there's no `.xinitrc`
    - because `startx` falls back on the same session startup scripts used for GUI login
- executed relatively early, before starting any program such as key-agent, D-Bus daemon etc.
- typically sets up variables that may be used by later startup scripts

> **NOTE:** You may use the `.xsessionrc` to startup programs (like `nm-applet`) and setup X11 resources (e.g. `xrdb -merge`) if you want to be DE/WM agnostic for autostart.

### <a name="xprofile"></a> The `~/.xprofile`

- very similar to `.xsessionrc` but exclusively used by GDM (Gnome Display Manager)

### <a name="xsession"></a> The `~/.xsession`

- executed when logging in to graphical mode (using a Display Manager) _and_ the `custom` session type is selected
- its role is to both set login-time parameters and to start GUI session
- a typical `.xsession` is
    ```bash
    #!/bin/sh
    . ~/.profile
    . ~/.xinitrc
    ```


### <a name="profilerc"></a> The `~/.profile`

- called by the default shell upon login
    - login here means when a new shell is spawned, which is usually the case for each terminal window opened

> **WARNING:** Since `~/.profile` is executed for each shell session (e.g. when you open a new terminal window or tab), it is not recommended to put any autostart commands in here!