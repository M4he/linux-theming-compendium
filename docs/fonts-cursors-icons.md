This chapter will explain how to configure fonts, cursors and icons independently of any desktop environment (DE).
For DEs that already offer configuration options but don't cover all use cases (e.g. not covering Qt or GTK applications respectively), you may use parts of the following instructions to fill the gaps where necessary.

> **WARNING:** Some parts in this chapter specify settings for `xsettingsd`.
It is not advised to use `xsettingsd` on desktop environments that already provide a XSETTINGS interface to prevent conflicts.
This includes all major ones, such as GNOME, KDE, Xfce, MATE, Budgie.
See the [corresponding section](theming.md#xsettings-interface) for more details.

## <a name="fontconfig"></a> Font Configuration

The following files affect the *font rendering* settings (hinting, DPI):

- [global] the `Xft.*` entries in `~/.Xdefaults`, `~/.Xresources`
    - plus anything loaded with `xrdb -merge`
- [global] `~/.config/fontconfig/fonts.conf`
- [GTK] the `Xft/*` settings of the [`xsettingsd` configuration](theming.md#xsettingsd-config)
- [Qt5] the [`QT_FONT_DPI` environment variable](theming.md#qt5-envvars)

See the [fontconfig setup example](appendix.md#fontconfig-example) in the appendix for an example about the above settings.

---

The following files affect the *font choice* (font name, size):

- [Qt5] settings of [qt5ct](theming.md#qt5ct) stored in `~/.config/qt5ct/qt5ct.conf`
- [GTK] the `Gtk/FontName` setting of the [`xsettingsd` configuration](theming.md#xsettingsd-config)
- [GTK] `gtk-font-name` in `~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings.ini`
    - (settings in `~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings.ini` are overruled by `Gtk/FontName` if using `xsettingsd`)

It is essential to keep these settings in sync to get the best results.
For an example of font rendering settings, see below.


## Cursor theme configuration

The following files affect the cursor size and theme settings:

- the `Xcursor.*` entries in `~/.Xdefaults`, `~/.Xresources` + anything loaded with `xrdb -merge`
- the `Gtk/CursorTheme*` settings of the [`xsettingsd` configuration](theming.md#xsettingsd-config)
- `gtk-cursor-theme-size` and `gtk-cursor-theme-name` in `~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings.ini`
    - (settings in `~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings.ini` are overruled by `xsettingsd`)

As with the font configuration it is essential to keep these settings in sync.

### Example cursor settings

The following settings show proper settings in sync with each other.

#### Entries in `~/.Xdefaults` or `~/.Xresources`

> **NOTE:** Changing cursor settings here requires a restart of your desktop session.

```
Xcursor.size: 32
Xcursor.theme: Breeze
```

#### xsettingsd

```
Gtk/CursorThemeSize 32
Gtk/CursorThemeName "Breeze"
```

## Icon themes

Not much to say here.
Use [xsettingsd](theming.md#xsettingsd-config) (applying to GTK) and [qt5ct](theming.md#qt5ct) (applying to Qt5) to set icon themes if your environment doesn't provide any means to do so.

User-specific icon themes should be put into `~/.local/share/icons/` rather than `~/.icons/` for better compatibility with KDE and Qt.

### Chaining icon themes together

Using the `Inherits` flag in an icon theme's `index.theme` file, you can specify other icon themes as a fallback in order of decreasing priority.
If an icon is not part of the icon theme, it will try to retrieve it from the themes specified in `Inherits`, choosing the first theme that provides the icon.
For example, the `index.theme` file of the Papirus icon theme reads the following:

```
[Icon Theme]
Name=Papirus
Comment=Papirus icon theme
Inherits=breeze,hicolor
...
```

This means that any requested icon not found in Papirus itself, will lead to a lookup of the icon in the Breeze icon theme and so on.
Using this technique you can mix and match icon themes or override parts of it when done right.

### Changing icons of single applications

> **NOTE:** There are limitations to this method.
Only application starters like start menus, rofi or similar tools will pick up this change.
It will often not reflect on panel taskbars or window titlebars, see [this FAQ entry](FAQ.md#faq-x11-icons).

You can change icons for specific applications by overriding the `Icon=` entry in the application's `.desktop` file.
The name you enter there will then be looked up in the currently configured icon theme.

Usually, those files reside in `/usr/share/applications/` but it is best to create a local copy in `~/.local/share/applications`:

```
cp /usr/share/applications/<name>.desktop ~/.local/share/applications/<name>.desktop
```

GUI tools like `menulibre` or `kmenuedit` can also do that for you when you modify an existing application and will create local copies in `~/.local/share/applications/` as well.

Any `.desktop` file in `~/.local/share/applications/` that has an exactly matching name to one in `/usr/share/applications/` will override the latter completely for most desktop components, like runners, start menus and taskbars.
