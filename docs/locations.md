Use the following general overview to identify where to look for certain theme and configuration paths.
Specific elements will be discussed in dedicated sections later on.

## Themes

<!-- - `~/.icons/*`
    - Icon themes and cursor themes.
    - Only available to the user (1).
    - Bad KDE/Qt compatibility.
- `~/.local/share/icons/*`
    - Icon themes and cursor themes.
    - Only available to the user (1).
    - Better KDE/Qt compatibility than `~/.icons/`. -->

|             Location             |                                                                                Purpose                                                                                |
|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `~/.icons/*`                     | Icon themes and cursor themes. Only available to the user (1). Bad KDE/Qt compatibility.                                                                              |
| `~/.local/share/icons/*`         | Icon themes and cursor themes. Only available to the user (1). Good KDE/Qt compatibility.                                                                             |
| `~/.themes/*`                    | GTK2/3, xfwm4, metacity, openbox themes. Only available to the user (1).                                                                                              |
| `~/.local/share/themes/*`        | GTK2/3, xfwm4, metacity, openbox themes. Only available to the user (1). Not properly supported by all environments, consider using `~/.themes/*`.                    |
| `/usr/share/icons/*`             | Icon themes and cursor themes. System-wide.                                                                                                                           |
| `/usr/share/themes/*`            | GTK2/3, xfwm4, metacity, openbox themes. System-wide.                                                                                                                 |
| `~/.local/share/lxqt/themes/*`   | LXQt themes (panel et al.). Only available to the user.                                                                                                               |
| `/usr/share/lxqt/themes/*`       | LXQt themes (panel et al.). System-wide.                                                                                                                              |
| `~/.local/share/color-schemes/*` | KDE color schemes for Breeze et al., effectively copied into `~/.config/kdeglobals` by KDE/Plasma when applied. Created/downloaded by and only available to the user. |
| `/usr/share/color-schemes/*`     | KDE color schemes for Breeze et al., effectively copied into `~/.config/kdeglobals` by KDE/Plasma when applied. System-wide.                                          |
| `/usr/share/plymouth/themes/*`   | Boot screen themes for Plymouth.                                                                                                            |
| `/usr/share/sddm/themes/`        | Login screen themes for the SDDM display manager.                                                                                                                     |

(1) GTK and cursor themes only available to the user might not be displayed correctly by applications that elevate their rights to root with a password prompt (policykit), like `gparted`, `synaptic` etc.
It is advised to store themes and icons system-wide. Also cursor themes might not correctly apply in some applications (e.g. Thunderbird) if they are *not* installed system-wide!

## Configuration files

|                Location                |                                                                    Purpose                                                                     |
|----------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `~/.Xdefaults`, `~/.Xresources`        | Common X11 configuration file paths for setting some font and cursor settings. Loaded by some DMs and can be used with `xrdb -merge` directly. |
| `~/.xsettingsd`                        | Default path for `xsettingsd` settings.                                                                                                        |
| `~/.config/xsettingsd/xsettingsd.conf` | `xsettingsd` configuration file generated and used by KDE/Plasma to sync its theme settings to GTK applications.                               |
| `~/.gtkrc-2.0`                         | GTK2 settings loaded by GTK2 applications if no XSETTINGS service (e.g. `xsettingsd`) is running.                                              |
| `~/.config/gtk-3.0/settings.ini`       | GTK3 settings loaded by GTK3 applications if no XSETTINGS service (e.g. `xsettingsd`) is running.                                              |
| `~/.config/gtk-3.0/gtk.css`            | GTK3 CSS overrides loaded by GTK3 applications.                                                                                                |
| `~/.config/kdeglobals`                 | Contains various KDE/Plasma 5 settings *and* the active KDE color scheme.                                                                      |
| `~/.config/qt5ct/`                     | Configuration path for qt5ct (see the [qt5ct section](theming.md#qt5ct)).                                                                      |
