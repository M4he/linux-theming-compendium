This is a reference for theming Linux systems on X11/Xorg environments based on personal experience.

> **NOTE:** This is not a step-by-step guide for theming (aka "ricing") your entire system.
This document is just a record of various bits and pieces about theming on X11/Xorg environments, gathering useful tweaks and information on common pitfalls in one place.
Do not take any configuration presented in this reference as the best and only way to do things. Feel free to experiment!

**Disclaimer:** This reference guide aims to be environment-agnostic and focuses on settings/tweaks outside of the big desktop environments (DEs).
Such environments often provide their own means of configuring things and might conflict with instructions in this document, so exercise caution!
Some tips might still apply to those nonetheless.

Lastly, if you have anything to add or spotted a mistake in this reference guide, please open a merge request or issue at [https://gitlab.com/M4he/linux-theming-compendium](https://gitlab.com/M4he/linux-theming-compendium).

## Document Structure

This document will start by giving a rough overview over the [layers of a X11/Xorg Linux desktop](#linux-layers) and will then proceed with the following chapters:

- [File Locations](locations.md) lists common theming directories and configuration file paths
- [Fonts, Cursors & Icons](fonts-cursors-icons.md) explains the basics about font, cursor and icon theme settings
- [GTK & Qt Theming Basics](theming.md) explains the basics about GTK and Qt theming
- [Advanced Tweaks](advanced.md) will touch on some more complex topics like GTK3 CSS, per-session theming and X11 session startup files
- [Login & Boot Screen](login-boot.md) briefly summarizes the core aspects of login and boot screen theming
- [Troubleshooting & FAQ](FAQ.md) is a FAQ-style collection of common pitfalls and quirks encountered during theming
- [Appendix](appendix.md) contains example files for some of the instructions from the other chapters

## <a name="linux-layers"></a> Linux X11/Xorg Desktop Component Structure

The rough layer structure from top to bottom for a X11/Xorg Linux desktop is:

|        Component         |                                  Common Examples                                  |                                                                             Purpose                                                                              |
|--------------------------|-----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GUI Toolkit              | GTK, Qt                                                                           | Used by applications to render their interface.                                                                                                                  |
| Compositor               | picom, Mutter (GNOME), KWin (KDE), Muffin (Cinnamon), Xfwm4 (Xfce)                | Enables smooth graphics, effects like transparency, shadows and animations for windows. For larger desktop environments, it is often part of the window manager. |
| Window Manager (WM)      | Openbox, awesome, i3, Mutter (GNOME), KWin (KDE), Muffin (Cinnamon), Xfwm4 (Xfce) | Manages windows and their titlebars.                                                                                                                             |
| Desktop Environment (DE) | GNOME, KDE/Plasma, Cinnamon, Xfce4, LXQt, Budgie                                  | Sets up the environment like key bindings, autostart, environment variables, theming etc.                                                                        |
| Display Manager (DM)     | GDM, SDDM, LightDM                                                                | Displays the login screen and starts the DE or WM session.                                                                                                       |

> **NOTE:** DEs are optional, you can usually run dedicated WMs standalone, like Openbox and awesome.
